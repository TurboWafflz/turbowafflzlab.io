import os
from tqdm import tqdm
import re
import sys

if "--dds" in sys.argv:
	#Get all dds files in mod
	os.system("find . -name '*.dds' > .ddsFiles.tmp")
	files = open(".ddsFiles.tmp", "r").readlines()
	print("Converting dds files to OpenGL format...")
	for file in tqdm(files):
		#Strip newlines from find output
		file = file.replace("\n", "")
		#Convert DirectX texture to OpenGL
		os.system("convert -quiet {} {} > /dev/null 2> /dev/null".format(file,file))

if "--jobs" in sys.argv:
	if not os.path.exists("def/vehicle/truck_company"):
		os.mkdir("def/vehicle/truck_company")
	#Get config sii files
	os.system("find ./def/vehicle/truck_dealer -name '*.sii' > .truckConfigFiles.tmp")
	configs = open(".truckConfigFiles.tmp", "r").readlines()
	print("Generating quick jobs...")
	for config in tqdm(configs):
		#Strip newlines from find output
		config = config.replace("\n", "")
		#Get text of dealer config
		configText = open(config).read()
		#Set the sii file as a quick job definition
		configText = re.sub(r"vehicle: \.[^\n]*", "vehicle: .company.truck", configText)
		#Write quick job configuration
		jobConfig = config.split("/")[-1]
		jobConfig = "./def/vehicle/truck_company/{}".format(jobConfig)
		open(jobConfig, "w+").write(configText)

if not "--dds" in sys.argv and not "--jobs" in sys.argv:
	print("You must specify whether to convert dds files (--dds), create quick jobs (--jobs), or both")