---
title: "A Comparison of several RPN and algebraic calculators"
date: 2023-01-22T13:52:11-05:00
description: "I compared the efficiency and ease of using RPN calculators to that of algebraic calculators. I think I understand why people like RPN now."
draft: false
---

Recently, I bought an HP 39gs graphing calculator, giving me an opportunity to try out [newRPL](https://newrpl.wiki.hpgcc3.org/doku.php), an excellent open source reimplementation of HP's RPL language used on their older calculators. While trying to learn how to better use this and other RPN calculators, I came across an [an old HP quiz](http://h20331.www2.hp.com/hpsub/downloads/S03_RPN_Quiz_V3.pdf) to test your skill at using RPN calculators, which featured this equation:

![Mach equation](/images/mach.png)

Although it looks intimidating, it was fairly simple to do and only took me two tries to properly calculate using newRPL. This made me curious, however, how different it would be to do on other calculators, so I tested it on several more, getting the following results (ordered from most to least keypresses):

#### HP 12c Platinum (Algebraic mode): 76 kepresses

350 / 661.5 [ENTER] [g] [x^2] * .2 + 1 [ENTER] ^ 3.5 - 1 [ENTER] [STO] 1 1 - [g] ( 6.875 [EEX] [CHS] 6 * 25500 [g] ) [ENTER] ^ 5.2656 [CHS] [ENTER] * [RCL] 1 [ENTER] + 1 [ENTER] ^ .286 - 1 [ENTER] * 5 [ENTER] [g] [sqrt]

#### TI-84 Plus Silver Edition (Algebraic, Mathprint on): 72 keypresses

[2nd] [sqrt] 5 ( ( ( ( ( 1 + .2 ( 350 / 661.5 ) [x^2] ) ^ 3.5 [->] - 1 ) ( 1 - 6.875 [2nd] [EE] [(-)] 6 * 25500 ) ^ [(-)] 5.2656 [->] ) + 1 ) ^ .286 [->] - 1

#### Casio FX-85 (Algebraic): 71 keypresses

350 / 661.5 = [INV] [x^2] * .2 + 1 = [INV] ^ 3.5 - 1 = [M in]  1 - 6.875 [EXP] [+/-] 6 * 25500 = [INV] ^ 5.2656 [+/-] * [MR] + 1 = [INV] ^ .286 - 1 = * 5 = [sqrt]


#### HP 39gs (newRPL): 65 keypresses

350 [ENTER] 661.5 / [right shift] [x^2] .2 * 1 + 3.5 ^ 1 - 6.875 [EEX] [space] [+/-] [backspace] 6 [ENTER] 25500 * 1 [ENTER] [->] - 5.2656 [+/-] ^ * 1 + .286 ^ 1 - 5 * [sqrt]

#### HP 12c Platinum (RPN mode): 63 keypresses

350 [ENTER] 661.5 / [g] [x^2] .2 * 1 + 3.5 ^ 1 - 6.875 [EEX] [CHS] 6 [ENTER] 25500 * 1 [x<->y] - 5.2656 [CHS] ^ * 1 + .286 ^ 1 - 5 * [g] [sqrt]

As you can see, the algebraic calculators took significantly more keypresses, but the real difficulty was actually just figuring out how to do the equation properly. Despite the Casio supporting enough levels of parentheses, typing the equation in properly was almost impossible without writing out what keys to press beforehand, and the lack of a stack meant that it was a lot easier to destroy all of your progress and have to restart. To make matters worse, the HP 12c in algebraic mode only supports one level of parentheses, so typing the equation as it appeared was completely impossible.

The solution I ended up using on both of these, which had the added benefit of reducing the number of keypresses required, was rearranging the first part in the innermost square brackets to not use parentheses, then storing that in the calculator's memory, and recalling it later when it needs to be multiplied by the second part. Although this did make it much easier to calculate, it still required at least 6 more keypresses than RPN, in addition to the time it took to just figure out the right way to do it.

I was kind of doubtful of the advantages of RPN calculators before this, but actually spending the time to learn how to use them and compare them to algebraic calculators like this has given me a newfound appreciation for them, and I think I'll start using them a lot more in the future.
